<?php


class Adapter implements LibraryOne
{
    /**
     * @var LibraryTwo $libraryTwo
     */
    protected $libraryTwoInstance;

    function __construct(LibraryTwo $libraryTwoInstance)
    {
        $this->libraryTwoInstance = $libraryTwoInstance;
    }

    public function doAB()
    {
        $this->libraryTwoInstance->doA();
        $this->libraryTwoInstance->doB();
    }
}