<?php


class LibraryTwoImpl implements LibraryTwo
{

    public function doA()
    {
        echo 'doA' . PHP_EOL;
    }

    public function doB()
    {
        echo 'doB' . PHP_EOL;
    }
}